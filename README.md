# RainbowParens

This is a \*nix-style filter to add color or other indicators to parentheses on
stdout. Currently, I prefer the `--colorize` mode, which adds color over the entire body of the parentheses, rather than just the parentheses themselves.

## Usage:
~~~
<some command that makes output> | rainbow.py
~~~
