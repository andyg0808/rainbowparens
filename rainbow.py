#!/usr/bin/env python3
"""
Prints text with rainbow delimiters

Usage:
    rainbow.py [options] [<file>...]

Options:
    --icons  Replace parentheses with icons.
    --numbers  Replace parentheses with their number.
    --colorize  Color all text between parentheses [Default: True]
    --invert  Colorize background rather than foreground.
"""
import docopt
from rainbowparens import Replacer
import fileinput



if __name__ == "__main__":
    opts = docopt.docopt(__doc__)
    replacer = Replacer(
        icons=opts["--icons"],
        numbers=opts["--numbers"],
        colorize=opts["--colorize"],
        invert=opts["--invert"],
    )
    for line in fileinput.input(opts["<file>"]):
        print(replacer.color(line), end="")
