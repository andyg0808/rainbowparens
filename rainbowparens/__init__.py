"""
Mark paired parentheses in text

This makes it easier to read text with many parentheses.
"""
import re
from functools import partial
from itertools import cycle

def attr(action):
    if action == 0:
        return "\033[39;49m"
    else:
        raise RuntimeError(f"Unknown attr value: {action}")


def bg(color):
    if color < 10:
        return f"\033[4{color}m"
    else:
        return f"\033[10{color%10}m"


def fg(color):
    if color < 10:
        return f"\033[3{color}m"
    else:
        return f"\033[9{color%10}m"


# Paren = re.compile(r"(\(|\[|\})|\)|\]|\}")
Paren = re.compile(r"(\()|\)")


def colorizer(foreground, background):
    return fg(foreground) + bg(background)


class ColorManager:
    def __init__(self):
        self.color_stack = []
        self.in_use = []
        self.color = cycle([1, 2, 3, 4, 5, 6, 11, 15])

    def get(self):
        if self.color_stack:
            color = self.color_stack.pop()
        else:
            color = next(self.color)
        self.in_use.append(color)
        return color

    def discard(self):
        if self.in_use:
            color = self.in_use.pop()
            self.color_stack.append(color)
            return color
        return 9

    def current(self):
        if self.in_use:
            return self.in_use[-1]
        return 9


class Replacer:
    def __init__(self, icons=False, numbers=False, colorize=False, invert=False):
        self.icons = icons
        self.numbers = numbers
        self.colors = ColorManager()
        self.idx = 0
        self.colorize = colorize
        self.invert = invert

    def get_text(self, match):
        if match[1]:
            self.idx += 1
        text = match[0]
        if self.icons:
            text = self.get_icon(match)
        elif self.numbers:
            text = self.get_number(match)
        else:
            text = match[0]
        if not match[1] and self.idx > 0:
            self.idx -= 1
        return text

    def get_color(self, match):
        if match[1]:
            return self.colors.get()
        elif self.colorize:
            self.colors.discard()
            return self.colors.current()
        else:
            return self.colors.discard()

    def get_number(self, match):
        if match[1] or self.idx > 0:
            return self.idx
        else:
            return 0

    def get_icon(self, match):
        return chr(0xf45d + self.idx) + " "

    def color_paren(self, match):
        """
        Takes a match object from the Paren regex and replaces it with a colored
        parenthesis.
        """
        color = self.get_color(match)
        replacement = self.get_text(match)
        if self.invert:
            if color == "black":
                colorize = partial(colorizer, background="white")
            else:
                colorize = partial(colorizer, "white")
        else:
            colorize = fg
        if self.colorize:
            if match[1]:
                return colorize(color) + str(replacement)
            else:
                return str(replacement) + colorize(color)
        else:
            return colorize(color) + str(replacement) + attr(0)

    def color(self, s: str) -> str:
        """
        Convert parentheses to colored parentheses
        """
        return Paren.sub(self.color_paren, s)
