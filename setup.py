# Most of this is taken from the docs at
# https://packaging.python.org/tutorials/packaging-projects/
import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="rainbowparens",
    version="0.0.6",
    author="Andrew Gilbert",
    author_email="1000temp2009@gmail.com",
    description="A package to add color to parenthesized text on stdout.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/andyg0808/rainbowparens",
    packages=setuptools.find_packages(),
    install_requires=["docopt"],
    scripts=["rainbow.py"],
    classifiers=(
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ),
)
